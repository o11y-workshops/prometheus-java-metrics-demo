FROM openjdk:19
ADD target/java_metrics-1.0-SNAPSHOT-jar-with-dependencies.jar java_metrics-1.0-SNAPSHOT-jar-with-dependencies.jar
ENTRYPOINT ["java", "-jar","java_metrics-1.0-SNAPSHOT-jar-with-dependencies.jar"]
EXPOSE 7777