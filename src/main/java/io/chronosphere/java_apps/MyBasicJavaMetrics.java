package io.chronosphere.java_apps;

import io.prometheus.metrics.exporter.httpserver.HTTPServer;
import io.prometheus.metrics.instrumentation.jvm.JvmMetrics;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * This is a basic Java example of how to get started instrumenting
 * a Java class that could be a service or application. A HTTP
 * server is started to expose an endpoint on the host at port 9999.
*/
public class MyBasicJavaMetrics {
 
    private static final int METRICS_PORT = 9999;

    public static void main(String[] args) throws InterruptedException, IOException {

        // Set up and default Java metrics.
        //JvmMetrics.builder().register();

        // Initialize Counter.
        // TODO: counter init
       
        // Initialized Gauge.
        // TODO: gauge init

        // Initialize Histogram.
        // TODO: histogram init
        
        // Initialized Summary.
        // TODO: summary init

         // Start thread and apply values to metrics.
        Thread bgThread = new Thread(() -> {
            while (true) {
                try {
                    // TODO: set up populate metrics.

                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        bgThread.start();


        HTTPServer server = HTTPServer.builder()
                .port(METRICS_PORT)
                .buildAndStart();

        System.out.println("HTTPServer listening on port http://localhost:" + server.getPort() + "/metrics");
        System.out.println("");
        System.out.println("Basic Java metrics setup successful...");
        System.out.println("");

        // Insert your code here for application or microservice.
        System.out.println("My application or service started...");
        System.out.println("");
    }
}
