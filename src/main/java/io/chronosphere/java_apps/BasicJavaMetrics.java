package io.chronosphere.java_apps;
  
import io.prometheus.metrics.core.metrics.Counter;
import io.prometheus.metrics.core.metrics.Gauge;
import io.prometheus.metrics.core.metrics.Histogram;
import io.prometheus.metrics.core.metrics.Summary;
import io.prometheus.metrics.exporter.httpserver.HTTPServer;
import io.prometheus.metrics.model.snapshots.Unit;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * This is a basic Java example of how to get started instrumenting
 * a Java class that could be a service or application. A HTTP
 * server is started to expose an endpoint on the host at port 9999.
*/
public class BasicJavaMetrics {
 
    private static final int METRICS_PORT = 7777;

    public static void main(String[] args) throws InterruptedException, IOException {

        // Set up and default Java metrics.
        //JvmMetrics.builder().register(); // uncomment to see all JVM metrics.

        // Initialize Counter.
        Counter counter = counter();
        
        // Initialize Gauge.
        Gauge gauge = gauge();

        // Initialize Histogram.
        Histogram histogram = histogram();
        long start = System.nanoTime();

        // Initialize Summary.
        Summary summary = summary();

        // Start thread and apply values to metrics.
        Thread bgThread = new Thread(() -> {
            while (true) {
                try {
                    counter.labelValues("ok").inc();
                    counter.labelValues("ok").inc();
                    counter.labelValues("error").inc();
                    gauge.labelValues("value").set(rand(-5, 10));
                    histogram.labelValues("GET", "/", "200").observe(Unit.nanosToSeconds(System.nanoTime() - start));
                    summary.labelValues("ok").observe(rand(0, 5));
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        bgThread.start();

        // Set up web server.
        HTTPServer server = HTTPServer.builder()
                .port(METRICS_PORT)
                .buildAndStart();

        System.out.println("HTTPServer listening on port http://localhost:" + server.getPort() + "/metrics");
        System.out.println("");
        System.out.println("Basic Java metrics setup successful...");
        System.out.println("");

        // Insert your code here for application or microservice.
        System.out.println("My application or service started...");
        System.out.println("");        
    }
    
    // Creating a counter.
    private static Counter counter() {
        Counter counter = Counter.builder()
                .name("java_app_c")
                .help("example counter")
                .labelNames("status")
                .register();

        return counter;
    }

    // Creating a gauge.
    private static Gauge gauge() {
        Gauge gauge = Gauge.builder()
                .name("java_app_g")
                .help("is a gauge metric")
                .labelNames("value")
                .unit(Unit.SECONDS)
                .register();

        return gauge;
    }

    // Creating a histogram.
    private static Histogram histogram() {
        Histogram histogram = Histogram.builder()
            .name("java_app_h")
            .help("is a histogram metric")
            .unit(Unit.SECONDS)
            .labelNames("method", "path", "status_code")
            .register();

        return histogram;
    }

    // Creating a summary.
    private static Summary summary() {
        Summary summary = Summary.builder()
                .name("java_app_s")
                .help("is summary metric (request latency in seconds)")
                .unit(Unit.SECONDS)
                .quantile(0.5, 0.01)
                .quantile(0.95, 0.005)
                .quantile(0.99, 0.005)
                .labelNames("status")
                .register();

        return summary;
    }
 
    // Creating a more random number.
    private static double rand(double min, double max) {
        return min + (Math.random() * (max - min));
    }
}