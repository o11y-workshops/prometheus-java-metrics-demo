# Simple Prometheus Metrics Instrumentation Java Example

This is a simple Java based example of how to export 4 types of metrics with Prometheus.

Requirements:  Java 8+, Maven

## Build example Java application

```bash
  mvn clean install

  java -jar target/java_metrics-1.0-SNAPSHOT-jar-with-dependencies.jar
```

Creating a container image (use podman or docker):

```bash
  # Build the container image.
  podman build -t basic-java-metrics -f Buildfile

  # Run image mapping from 7777 local machine port to the
  # exposed 7777 port on running container.
  podman run -p 7777:7777 basic-java-metrics
```

## Running application

View metrics:  <http://localhost:7777/metrics>

 ```text
# HELP java_app_c_total example counter
# TYPE java_app_c_total counter
java_app_c_total{status="error"} 239.0
java_app_c_total{status="ok"} 478.0
# HELP java_app_g_seconds is a gauge metric
# TYPE java_app_g_seconds gauge
java_app_g_seconds{value="value"} 7.29573889110867
# HELP java_app_h_seconds is a histogram metric
# TYPE java_app_h_seconds histogram
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="0.005"} 0
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="0.01"} 0
...
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="10.0"} 10
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="+Inf"} 239
java_app_h_seconds_count{method="GET",path="/",status_code="200"} 239
java_app_h_seconds_sum{method="GET",path="/",status_code="200"} 28475.853574282995
# HELP java_app_s_seconds is summary metric (request latency in seconds)
# TYPE java_app_s_seconds summary
java_app_s_seconds{status="ok",quantile="0.5"} 2.870230936180606
java_app_s_seconds{status="ok",quantile="0.95"} 4.888056778494996
java_app_s_seconds{status="ok",quantile="0.99"} 4.903344773262025
java_app_s_seconds_count{status="ok"} 239
java_app_s_seconds_sum{status="ok"} 607.9779550254922
 ```

## Prometheus configuration to scrape metrics

Add this to a Prometheus scrape configuration to enable metrics collection:

```yaml
scrape_configs:

# Scraping Java metrics.
  - job_name: "java_app"
    static_configs:
      - targets: ["localhost:7777"]
        labels:
          job: "java_app"
          env: "workshop-lab8"
```

If you are running Prometheus in a container and build the Java app to run in a container, localhost:7777 will not resolve. You'll need to find your local container IP for Prometheus to 
target as follows:

```bash
$ ifconfig | grep "inet " | grep -Fv 127.0.0.1 | awk '{print $2}'

192.168.1.4
```

This results can then be applied to your Prometheus scrape configuration to enable metrics collection:

```yaml
scrape_configs:

# Scraping Java metrics.
  - job_name: "java_app"
    static_configs:
      - targets: ["192.168.1.4:7777"]
        labels:
          job: "java_app"
          env: "workshop-lab8"
```

## Implementation details for counter

This represents a cumulative metric that only increases over time, like the number of requests to an endpoint. Later you can query how fast the value is increasing using rate function. Some of the cases where you would use a counter:

- Number of requests processed

- Number of items that were inserted into a queue

- Total amount of data a system has processed

- Error Count

The counter used in this example is the `java_app_c_total` metric for both OK and ERROR totals:

```text
# HELP java_app_c_total example counter
# TYPE java_app_c_total counter
java_app_c_total{status="error"} 239.0
java_app_c_total{status="ok"} 478.0
```

The best way to query this in the console using PromQL is to look at the increase over time using the `rate()` function, such as:

```sql
rate(java_app_c_total[10m])
```

## Implementation details for gauge

Gauges are instant measurements of a value. Gauges represent a random value that can increase and decrease randomly such as the load of your system. Some cases for using gauges:

- In progress requests

- Number of items in a queue

- Free memory

- Total memory

The gauge used in this example is the `java_app_g_seconds` metric:

```text
# HELP java_app_g_seconds is a gauge metric
# TYPE java_app_g_seconds gauge
java_app_g_seconds{value="value"} 7.29573889110867
```

A gauge can both increase and decrease over time, so the best way to query is to use the `avg_over_time` function, such as:

```sql
avg_over_time(java_app_g_seconds[10m])
```

## Implementation details for histogram

A histogram samples observations (such as request durations or response sizes) and counts them in configurable buckets. It also provides a sum of all observed values which gives you an approximation. Rather than storing every duration for every request, Prometheus will make an approximation by storing the frequency of requests that fall into particular buckets. Some cases for using histograms:

- Response latency

- Request size

The histogram used in this example is the `java_app_h_seconds` metric and it exposes multiple time series during a scrape:

```text
# HELP java_app_h_seconds is a histogram metric
# TYPE java_app_h_seconds histogram
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="0.005"} 0
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="0.01"} 0
...
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="10.0"} 10
java_app_h_seconds_bucket{method="GET",path="/",status_code="200",le="+Inf"} 239
java_app_h_seconds_count{method="GET",path="/",status_code="200"} 239
java_app_h_seconds_sum{method="GET",path="/",status_code="200"} 28475.853574282995
```

The best way to query it is with the `rate()` function and over time to see the duration of something over time:

```sql
rate(java_app_h_seconds_sum[5m]) / rate(java_app_h_seconds_count[5m])
```

For an advanced query see the use of the `histogram_quantile()` function:

```sql
histogram_quantile(0.95, sum(rate(java_app_h_seconds_bucket[5m])) by (le))
```

## Implementation details for summaries

Similar to a histogram, a summary samples observations. While it also provides a total count of observations and a sum
of all observed values, it calculates configurable quantiles over a sliding time window. Some cases for using summaries:

- Response latency

- Request size

- Request duration

The summary used in this example is the `java_app_s_seconds` metric that exposes multiple time series during a scrape (found in the Prometheus metrics) for request latency in seconds:

```text
# HELP java_app_s_seconds is summary metric (request latency in seconds)
# TYPE java_app_s_seconds summary
java_app_s_seconds{status="ok",quantile="0.5"} 2.870230936180606
java_app_s_seconds{status="ok",quantile="0.95"} 4.888056778494996
java_app_s_seconds{status="ok",quantile="0.99"} 4.903344773262025
java_app_s_seconds_count{status="ok"} 239
java_app_s_seconds_sum{status="ok"} 607.9779550254922
```

An example of querying a summary:

```sql
rate(java_app_s_seconds_sum[5m]) / rate(java_app_s_seconds_count[5m])
```

## Resources

- [O11y Guide - Beginners Guide to Open Source Instrumenting Java](https://www.schabell.org/2023/12/o11y-guide-beginners-guide-to-open-source-instrumenting-java.html)


## Releases

 v0.3 - Example Prometheus metrics based on Java client v1.3.1.

 v0.2 - Example Prometheus metrics based on Java client v1.0.0.

 v0.1 - Example Prometheus metrics based on Java simpleclient v0.6.
